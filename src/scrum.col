hADL "/Users/Christoph/TU/Diplomarbeit/Code/Evaluation/src/main/resources/agile-hadl.xml"

className net.laaber.mt.evaluation.Tasks
resourceDescriptorFactory agilefantRd class net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory
resourceDescriptorFactory atlassianRd class net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory
sensorFactory net.laaber.mt.evaluation.sensor.DslSensorFactory

task sprint {
	javaIn id : Integer
	javaIn name : String
	
	acquire scrum.obj.Sprint with agilefantRd.agilefant(name, id) as s
	
	out s as "sprint"
}

task createChatsForStoriesOfSprint {
	in s : scrum.obj.Sprint
	
	stopScope
	
	startingFrom s load scrum.obj.Story:scrum.obj.containsStory as stories
	
	var chats : [scrum.obj.Chat]
	var invites : [scrum.links.invite]
	for all stories as story counter i {
		acquire scrum.obj.Chat with atlassianRd.hipChat("StoryChat" + i, "StoryChat" + i) as c
		add c to chats
		
		startingFrom story via scrum.col.AgileUser:scrum.links.responsible load scrum.col.DevUser:scrum.col.devUser as users
		for all users as u {
			link u and c by scrum.links.invite as invite
			add invite to invites
		}
	}
	
	startScope
	
	out chats as "chats"
	out invites as "invites"
}

task releaseChatsAndInvites {
	in chats : [scrum.obj.Chat]
	in invites : [scrum.links.invite]
	
	stopScope
	
	for all invites as i {
		unlink i
	}
	
	startScope
	
	for all chats as c {
		release c
	}
}

task sprintRetrospectiveSetUp {
	in sprint : scrum.obj.Sprint
	javaIn sprintNumber : Integer
	javaIn previousSprintNumber : Integer
	
	stopScope
	
	acquire scrum.obj.Wiki with atlassianRd.bitbucket("SprintWiki" + sprintNumber, "SprintWiki" + sprintNumber) as currentWiki
	acquire scrum.obj.Wiki with atlassianRd.bitbucket("SprintWiki" + previousSprintNumber, "SprintWiki" + previousSprintNumber) as previousWiki
	reference from currentWiki to previousWiki with scrum.obj.prev as p1
	reference from previousWiki to currentWiki with scrum.obj.next as p2
	
	startScope
	
	acquire scrum.obj.Chat with atlassianRd.hipChat("RetrospectiveChat" + sprintNumber, "RetrospectiveChat" + sprintNumber) as chat
	startingFrom sprint via scrum.obj.Story:scrum.obj.containsStory via scrum.col.AgileUser:scrum.links.responsible load scrum.col.DevUser:scrum.col.devUser as users
	
	stopScope
	
	var invites : [scrum.links.invite]
	for all users as u {
		link u and chat by scrum.links.invite as i
		add i to invites
	}
	
	startScope
	
	release previousWiki
	
	out chat as "chat"
	out currentWiki as "wiki"
	out invites as "invites"
}

task sprintRetrospectiveTearDown {
	in chat : scrum.obj.Chat
	in invites : [scrum.links.invite]
	
	stopScope
	
	for all invites as i {
		unlink i
	}
	
	startScope
	
	release chat
}
