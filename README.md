# Scrum DSL Script for hADL

This repository creates collaboration instances of the [Scrum hADL Model](https://bitbucket.org/chrstphlbr/mt_evaluation_hadl). It is part of the [Evaluation](https://bitbucket.org/chrstphlbr/mt_evaluation) of Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis/).

In order to modify the collaborations follow the steps below:

* Get the [DSL project](https://bitbucket.org/chrstphlbr/mt_dsl) up and running.
* Follow the instructions there. Only difference is that you use this project instead of creating a new one.
* Don't forget to set the Java Build Path as described in the DSL project.
* Use the [Scrum hADL Model](https://bitbucket.org/chrstphlbr/mt_evaluation_hadl).

Caveat: Do not forget that the Scrum model is in incomplete. It is sufficient to use for editing the DSL script, but not for actually executing the collaborations. A complete hADL model is part of the [Evaluation](https://bitbucket.org/chrstphlbr/mt_evaluation) project in the folder `src/main/resources/agile-hadl.xml`.